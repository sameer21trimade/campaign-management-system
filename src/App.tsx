import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CampaignPageHeader from "./containers/CampaignPage/CampaignPageHeader/CampaignPageHeader";
import CampaignPageForm from "./containers/CampaignPage/CampaignPageForm/CampaignPageForm";
import "./App.css";
import { Provider } from "react-redux";
import store from "./Redux/store/Root/Index";
import Dashboard from "./containers/DashboardPage/Dashboard";


function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/campaignName/:campaignName" component={CampaignPageForm} />
          <Route
            path="/createCampaignID/:id/:campaignName/:userName/:userEmail/:subject/:emailTemplate/:mailerName"
            component={CampaignPageForm}
          />
          <Route path="/createCampaign" component={CampaignPageForm} />
          <Route path="/campaigns" component={CampaignPageHeader} />
          <Route path="/" component={Dashboard} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
