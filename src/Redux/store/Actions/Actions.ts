import { action } from "typesafe-actions";
import { Constants } from "../Types/Types";
export function addItemToList(
  campaignName: string,
  name: string,
  email: string,
  subject: string,
  emailTemplate: string,
  mailerName: string
) {
  return action(Constants.ADD_ITEM, {
    campaignName,
    name,
    email,
    subject,
    emailTemplate,
    mailerName,
  });
}
export function removeItemFromList(id: number) {
  return action(Constants.DELETE_ITEM, {
    id,
  });
}
export function editItemFromList(
  id: number,
  campaignName: string,
  name: string,
  email: string,
  subject: string,
  emailTemplate: string,
  mailerName: string
) {
  return action(Constants.EDIT_ITEM, {
    id,
    campaignName,
    name,
    email,
    subject,
    emailTemplate,
    mailerName,
  });
}
export function displayNotification(msg: boolean) {
  return action(Constants.NOTIFY, {
    msg,
  });
}
export function displayEditNotification(msg: boolean) {
  return action(Constants.EDIT_MSG, {
    msg,
  });
}
export function onChangeTab(value: boolean) {
  return action(Constants.SET_SELECTED_TAB, {
    value,
  });
}
export function fetchSelectedTabTitle(title: string) {
  return action(Constants.SELECTED_TAB_TITLE, {
    title,
  });
}
export function filterCampaigns(noCampaignFound: boolean) {
  return action(Constants.SEARCH_VALUE, {
    noCampaignFound,
  });
}
