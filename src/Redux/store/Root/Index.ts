import { combineReducers, createStore } from 'redux';
import { Reducer } from '../Reducers/Reducers';
import { InitialState } from '../Types/Types';
export interface InitialRootState {
    rootReducer: InitialState
}
const store = createStore<InitialRootState, any, any, any>(
    combineReducers({
        rootReducer: Reducer
}));
export default store;