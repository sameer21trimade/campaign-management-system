import { ActionType } from "typesafe-actions";
import * as actions from "../Actions/Actions";
export type Actions = ActionType<typeof actions>;
interface campaignDetails {
  id: number;
  date: string;
  time: string;
  campaignName: string;
  userName: string;
  userEmail: string;
  status: string;
  opens: number;
  clicks: number;
  subject: string;
  emailTemplate: string;
  mailerName: string;
}
export interface InitialState {
  list: campaignDetails[];
  notification: boolean;
  editMsg: boolean;
  onChangeTab: boolean;
  selectedTabTitle: string;
  searchInput: boolean;
}
export enum Constants {
  ADD_ITEM = "ADD_ITEM",
  DELETE_ITEM = "DELETE_ITEM",
  EDIT_ITEM = "EDIT_ITEM",
  NOTIFY = "NOTIFY",
  EDIT_MSG = "EDIT_MSG",
  SET_SELECTED_TAB = "SET_SELECTED_TAB",
  SELECTED_TAB_TITLE = "SELECTED_TAB_TITLE",
  SEARCH_VALUE = "SEARCH_VALUE",
}
