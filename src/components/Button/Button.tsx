import React, { Component } from "react";
import "./Button.scss";

interface props {
  buttonName: any;
  buttonStyle: any;
  buttonType: any;
  onClick: any;
}

const buttonStylesArray = [
  "button_primary",
  "button_secondary",
  "modal_primary",
  "modal_secondary",
  "clear"
];

class Button extends Component<props> {
  render() {
    const { buttonName, buttonStyle, buttonType, onClick } = this.props;
    const checkButtonStyle = buttonStylesArray.includes(buttonStyle)
      ? buttonStyle
      : buttonStyle[0];
    return (
      <>
        <button
          className={`btn ${checkButtonStyle}`}
          onClick={onClick}
          type={buttonType}
        >
          {" "}
          {buttonName}{" "}
        </button>
      </>
    );
  }
}

export default Button;
