import React, { Component } from "react";
import "./Header.scss";
import { NavLink } from "react-router-dom";
import extendedDrawer from "../../images/extendedDrawer.svg";

interface Header_State {
  gotoNextPage: boolean;
}
class Header extends Component<{}, Header_State> {
  constructor() {
    super({});
    this.state = {
      gotoNextPage: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    if (this.state.gotoNextPage) this.setState({ gotoNextPage: false });
    else this.setState({ gotoNextPage: true });
  }

  render() {
    const { gotoNextPage } = this.state;
    return (
      <>
        <div className="header">
          <h2>Campaign Management</h2>
          <img
            src={extendedDrawer}
            onClick={() => this.handleClick()}
            alt="extendedDrawer"
          />
          <div className="nav_link_desktop">
            <ul>
              <nav className="navbar">
                <NavLink
                  exact
                  activeClassName="navbar_link_active"
                  className="navbar_link"
                  to="/"
                >
                  <li>Dashboard</li>
                </NavLink>
                <NavLink
                  activeClassName="navbar_link_active"
                  className="navbar_link"
                  to="/campaigns"
                >
                  <li>Campaigns</li>
                </NavLink>
              </nav>
            </ul>
          </div>
        </div>
        <div className="nav_link_mobile">
          {gotoNextPage ? (
            <ul>
              <nav className="navbar">
                <NavLink
                  exact
                  activeClassName="navbar_link_active"
                  className="navbar_link"
                  to="/"
                >
                  <li>Dashboard</li>
                </NavLink>
                <NavLink
                  activeClassName="navbar_link_active"
                  className="navbar_link"
                  to="/campaigns"
                >
                  <li>Campaigns</li>
                </NavLink>
              </nav>
            </ul>
          ) : undefined}
        </div>
      </>
    );
  }
}

export default Header;
