import React, { Component } from "react";
import "./Input.scss";

interface props {
  onChange: any;
  placeholder: string;
  value: string;
  type: string;
  name: any;
}

class Input extends Component<props> {
  render() {
    const { onChange, placeholder, value, type, name } = this.props;
    return (
      <>
        <input
          className="input"
          type={type}
          name={name}
          onChange={onChange}
          placeholder={placeholder}
          value={value}
        />
      </>
    );
  }
}

export default Input;
