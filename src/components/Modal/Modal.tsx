import React, { Component } from "react";
import ReactModal from "react-modal";
import "./Modal.scss";

interface props {
  isOpen: boolean;
}

ReactModal.setAppElement("#root");
class Modal extends Component<props> {
  render() {
    const { isOpen, children } = this.props;
    return (
      <>
        <ReactModal
          isOpen={isOpen}
          style={{ overlay: { background: "#000000bf" } }}
          className="customStyles"
        >
          {children}
        </ReactModal>
      </>
    );
  }
}

export default Modal;
