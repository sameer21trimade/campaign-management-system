import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { InitialRootState } from "../../Redux/store/Root/Index";
import { Dispatch } from "redux";
import * as actions from "../../Redux/store/Actions/Actions";
import { Actions } from "../../Redux/store/Types/Types";
import "./Pagination.scss";
import Button from "../Button/Button";

type Props = {
  listItemsPerPage: number;
  totalLists: number;
  paginate: any;
  gotoPreviousPage: any;
  gotoNextPage: any;
};

interface PaginationInterface {
  pageNumbers: number[];
  checkIndexValue: number;
}

type ReduxType = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatcherToProps>;

class Pagination extends Component<Props & ReduxType, PaginationInterface> {
  constructor(props: Props & ReduxType) {
    super(props);
    this.state = {
      pageNumbers: [],
      checkIndexValue: 0,
    };
    this.setIndexValue = this.setIndexValue.bind(this);
    this.incrementIndexValue = this.incrementIndexValue.bind(this);
    this.decrementIndexValue = this.decrementIndexValue.bind(this);
  }

  componentDidMount() {
    let temporaryArray = [];
    for (
      let index = 1;
      index <= Math.ceil(this.props.totalLists / this.props.listItemsPerPage);
      index++
    ) {
      temporaryArray.push(index);
    }
    this.setState({ pageNumbers: temporaryArray });
  }

  componentDidUpdate() {
    if (this.props.onChangeTab) {
      let temporaryArray = [];
      for (
        let index = 1;
        index <= Math.ceil(this.props.totalLists / this.props.listItemsPerPage);
        index++
      ) {
        temporaryArray.push(index);
      }
      this.setState({ pageNumbers: temporaryArray });
      this.props.changePageNumber(false);
      this.setState({ checkIndexValue: 0 });
    }
  }

  setIndexValue(index: any) {
    this.setState({ checkIndexValue: index });
  }

  incrementIndexValue() {
    if (this.state.checkIndexValue < this.state.pageNumbers.length - 1)
      this.setState((prevstate) => ({
        checkIndexValue: prevstate.checkIndexValue + 1,
      }));
  }

  decrementIndexValue() {
    if (this.state.checkIndexValue > 0)
      this.setState((prevstate) => ({
        checkIndexValue: prevstate.checkIndexValue - 1,
      }));
  }

  render() {
    const { pageNumbers } = this.state;
    const { paginate, searchInput } = this.props;
    // console.log(this.props.searchInput)
    return (
      <>
        <nav className="pagination">
          <ul>
            { ( pageNumbers.length !== 0 ) && ( !searchInput ) ? (
              <>
                <li onClick={() => this.decrementIndexValue()}>
                  <Button
                    buttonName={<span>&#8656;</span>}
                    onClick={() => this.props.gotoPreviousPage()}
                    buttonType="button"
                    buttonStyle="button_primary"
                  />
                </li>
                {pageNumbers.map((number, index) => (
                  <li
                    key={number}
                    onClick={() => this.setIndexValue(index)}
                    className="page_numbers"
                  >
                    {this.state.checkIndexValue === index ? (
                      <NavLink
                        onClick={() => paginate(number)}
                        to="#"
                        className="page_link"
                        style={{ border: "1px solid #4490fb" }}
                      >
                        {number}
                      </NavLink>
                    ) : (
                      <NavLink
                        onClick={() => paginate(number)}
                        to="#"
                        className="page_link"
                      >
                        {number}
                      </NavLink>
                    )}
                  </li>
                ))}
                <li onClick={() => this.incrementIndexValue()}>
                  <Button
                    buttonName={<span>&#8658;</span>}
                    onClick={() => this.props.gotoNextPage(pageNumbers)}
                    buttonType="button"
                    buttonStyle="button_primary"
                  />
                </li>
              </>
            ) : (
              <li id="empty_state">No Data</li>
            )}
          </ul>
        </nav>
      </>
    );
  }
}

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    changePageNumber: (value: boolean) => dispatch(actions.onChangeTab(value)),
    searchValue: (noCampaignFound: boolean) =>
      dispatch(actions.filterCampaigns(noCampaignFound)),
  };
};

const mapStateToProps = ({ rootReducer }: InitialRootState) => {
  const { onChangeTab, searchInput } = rootReducer;
  return { onChangeTab, searchInput };
};

export default connect(mapStateToProps, mapDispatcherToProps)(Pagination);
