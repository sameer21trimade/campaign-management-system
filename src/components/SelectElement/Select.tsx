import React, { Component } from "react";
import './Select.scss'

interface props {
  onChange: any;
  defaultValue: string;
  disabledValue: string;
  optionArray: string[];
}

class Select extends Component<props> {
  render() {
    const { onChange, defaultValue, disabledValue, optionArray } = this.props;
    const result = optionArray.map((index) => {
      return <option>{index}</option>;
    });
    return (
      <>
        <select onChange={onChange} defaultValue={defaultValue}>
          <option disabled selected value="">
            {" "}
            {disabledValue}{" "}
          </option>
          {result}
        </select>
      </>
    );
  }
}

export default Select;
