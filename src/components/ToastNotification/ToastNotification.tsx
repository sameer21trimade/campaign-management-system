import React, { Component } from "react";
import ReactModal from "react-modal";

interface props {
  isOpen: boolean;
  style: any;
}

class Modal extends Component<props> {
  render() {
    const { children, style } = this.props;
    return (
      <>
        <div className="toast_container" style={style}>
          {children}
        </div>
      </>
    );
  }
}

export default Modal;
