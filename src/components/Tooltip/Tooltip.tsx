import React, { Component } from "react";
import "./Tooltip.scss";

interface props {
  tooltiptext: string;
}

class Tooltip extends Component<props> {
  render() {
    const { tooltiptext, children } = this.props;
    return (
      <>
        <div className="tooltip">
          {children}
          <span className="tooltiptext">{tooltiptext}</span>
        </div>
      </>
    );
  }
}

export default Tooltip;
