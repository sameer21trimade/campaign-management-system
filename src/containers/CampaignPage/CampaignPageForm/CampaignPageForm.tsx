import React, { Component } from "react";
import Header from "../../../components/Header/Header";
import "./CampaignPageForm.scss";
import { connect } from "react-redux";
import { InitialRootState } from "../../../Redux/store/Root/Index";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";
import NewForm from "./NewForm";
import EditExistingForm from "./EditExistingForm";

type ReduxType = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatcherToProps>;

interface RouteComponentProps {
  match: any;
  params: any;
}

interface NewCampaign_Component_State {
  top: number;
}

class NewCampaign extends Component<
  ReduxType & RouteComponentProps,
  NewCampaign_Component_State
> {
  constructor(props: ReduxType & RouteComponentProps) {
    super(props);
    this.state = {
      top: -100,
    };
  }

  componentDidMount() {
    if (this.props.notification) {
      this.setState({ top: 16 }, () => {
        setTimeout(() => {
          this.setState({ top: -100 });
          this.props.notificationMsg(false);
        }, 2000);
      });
    }
  }

  render() {
    return (
      <>
        <Header />
        {this.props.notification ? (
          <>
            <div className="toast_container" style={{ top: this.state.top }}>
              Camapign Created Successfully
            </div>
          </>
        ) : undefined}
        {this.props.match.params.userName &&
        this.props.match.params.userEmail ? (
          <EditExistingForm
            campaignName={this.props.match.params.campaignName}
            id={this.props.match.params.id}
            userName={this.props.match.params.userName}
            userEmail={this.props.match.params.userEmail}
            subject={this.props.match.params.subject}
            mailerName={this.props.match.params.mailerName}
            emailTemplate={this.props.match.params.emailTemplate}
          />
        ) : (
          <NewForm campaignName={this.props.match.params.campaignName} />
        )}
      </>
    );
  }
}

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    notificationMsg: (msg: boolean) =>
      dispatch(actions.displayNotification(msg)),
  };
};

const mapStateToProps = ({ rootReducer }: InitialRootState) => {
  const { notification } = rootReducer;
  return { notification };
};

export default connect(mapStateToProps, mapDispatcherToProps)(NewCampaign);
