import React, { Component } from "react";
import "./CampaignPageForm.scss";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";
import { NavLink } from "react-router-dom";
import Button from "../../../components/Button/Button";
import Input from "../../../components/InputElement/Input";
import Select from "../../../components/SelectElement/Select";

type ReduxType = ReturnType<typeof mapDispatcherToProps>;

interface props {
  campaignName: string;
  id: number;
  userName: string;
  userEmail: string;
  subject: string;
  mailerName: string;
  emailTemplate: string;
}

interface stateProperties {
  userName: string;
  userNameError: string;
  userEmail: string;
  userEmailError: string;
  inputText: string;
  emailTemplate: string;
  isFormValid: boolean;
  top: number;
  subject: string;
  mailerName: string;
}

class EditExistingForm extends Component<ReduxType & props, stateProperties> {
  constructor(props: ReduxType & props) {
    super(props);
    this.state = {
      userName: "",
      userNameError: "",
      userEmail: "",
      userEmailError: "",
      inputText: "",
      emailTemplate: "",
      isFormValid: false,
      top: -100,
      subject: "",
      mailerName: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.cancelEditOption = this.cancelEditOption.bind(this);
    this.inputEmailTemplate = this.inputEmailTemplate.bind(this);
    this.inputEmailTemplate = this.inputEmailTemplate.bind(this);
    this.inputMailerName = this.inputMailerName.bind(this);
    this.validateData = this.validateData.bind(this);
    this.validate = this.validate.bind(this);
  }

  handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(({
      [event.target.name]: event.target.value,
    } as unknown) as stateProperties);
    this.validateData();
  };

  inputEmailTemplate(event: React.ChangeEvent<HTMLSelectElement>) {
    this.setState({ emailTemplate: event.target.value });
  }

  inputMailerName(event: React.ChangeEvent<HTMLSelectElement>) {
    this.setState({ mailerName: event.target.value });
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    this.validate();
  }

  handleClick() {
    const isValid = this.validate();
    if (isValid) {
      this.props.editItem(
        this.props.id,
        this.props.campaignName,
        this.state.userName,
        this.state.userEmail,
        this.state.subject,
        this.state.emailTemplate,
        this.state.mailerName
      );
      this.props.fetchTitle("Recent");
    }
  }

  cancelEditOption() {
    this.props.notifyEditMsg(false);
    this.props.fetchTitle("Recent");
  }

  validateData() {
    if (this.state.userName.length > 0 && this.state.userEmail.includes("@")) {
      this.setState({ isFormValid: true });
    } else {
      this.setState({ isFormValid: false });
    }
  }

  validate() {
    let userNameError = "";
    let userEmailError = "";

    if (!this.state.userName) {
      userNameError = "*Name cannot be blank";
    }

    if (!this.state.userEmail.includes("@")) {
      userEmailError = "*Invalid email";
    }

    if (userEmailError || userNameError) {
      this.setState({ userEmailError: userEmailError });

      this.setState({ userNameError: userNameError });
      return false;
    }
    return true;
  }

  componentDidMount() {
    if (this.props.subject === "Not Applicable") this.setState({ subject: "" });
    else this.setState({ subject: this.props.subject });
    this.setState(
      {
        userName: this.props.userName,
        userEmail: this.props.userEmail,
        mailerName: this.props.mailerName,
        emailTemplate: this.props.emailTemplate,
      },
      () => {
        this.validateData();
      }
    );
  }

  render() {
    const { userNameError, userEmailError, isFormValid } = this.state;
    return (
      <>
        <div className="form_container">
          <form onSubmit={this.handleSubmit}>
            <h3>{this.props.campaignName}</h3>
            <h2>To:</h2>
            <div className="dropdown_container">
              <p>Mailer Name</p>
              {this.props.mailerName !== "Not Applicable" ? (
                <Select
                  onChange={(event: any) => this.inputMailerName(event)}
                  defaultValue={this.props.mailerName}
                  disabledValue="-- select a mailer name --"
                  optionArray={["AN-test1", "test1", "mailer2"]}
                />
              ) : (
                <Select
                  onChange={(event: any) => this.inputMailerName(event)}
                  defaultValue=""
                  disabledValue="-- select a mailer name --"
                  optionArray={["AN-test1", "test1", "mailer2"]}
                />
              )}
            </div>
            <h2>From:</h2>
            <div className="input_field_container border_bottom">
              <div className="required_fields">
                <p>
                  <span>Name</span>
                  <span className="warning">*</span>
                </p>
                <Input
                  onChange={(event: any) => this.handleOnChange(event)}
                  type="text"
                  name="userName"
                  placeholder="Enter name"
                  value={this.state.userName}
                />
                <small>{userNameError}</small>
              </div>
              <div className="required_fields">
                <p>
                  <span>Email</span>
                  <span className="warning">*</span>
                </p>
                <Input
                  onChange={(event: any) => this.handleOnChange(event)}
                  type="text"
                  name="userEmail"
                  placeholder="Enter email"
                  value={this.state.userEmail}
                />
                <small>{userEmailError}</small>
              </div>
            </div>
            <div className="input_field_container">
              <div>
                <p>Subject</p>
                {this.props.subject !== "Not Applicable" ? (
                  <Input
                    onChange={(event: any) => this.handleOnChange(event)}
                    type="text"
                    name="subject"
                    placeholder="Enter subject"
                    value={this.state.subject}
                  />
                ) : (
                  <Input
                    onChange={(event: any) => this.handleOnChange(event)}
                    type="text"
                    name="subject"
                    placeholder="Enter subject"
                    value={this.state.subject}
                  />
                )}
              </div>
              <div className="dropdown_container">
                <p>Email template</p>
                {this.props.emailTemplate !== "Not Applicable" ? (
                  <Select
                    onChange={(event: any) => this.inputEmailTemplate(event)}
                    defaultValue={this.props.emailTemplate}
                    disabledValue="-- select a template --"
                    optionArray={[
                      "Review email template",
                      "Invoice email template",
                      "Follow up email template",
                      "Sales email template",
                    ]}
                  />
                ) : (
                  <Select
                    onChange={(event: any) => this.inputEmailTemplate(event)}
                    defaultValue=""
                    disabledValue="-- select a template --"
                    optionArray={[
                      "Review email template",
                      "Invoice email template",
                      "Follow up email template",
                      "Sales email template",
                    ]}
                  />
                )}
              </div>
            </div>
            <div className="send_and_cancel_buttons">
              {isFormValid ? (
                <NavLink to="/campaigns">
                  <Button
                    buttonName="Send Campaign"
                    onClick={() => this.handleClick()}
                    buttonType="button"
                    buttonStyle="button_primary"
                  />
                </NavLink>
              ) : (
                <Button
                  buttonName="Send Campaign"
                  onClick={undefined}
                  buttonType="submit"
                  buttonStyle="button_primary"
                />
              )}
              <NavLink to="/campaigns">
                <Button
                  buttonName="Cancel"
                  onClick={() => this.cancelEditOption()}
                  buttonType="button"
                  buttonStyle="button_secondary"
                />
              </NavLink>
            </div>
          </form>
        </div>
      </>
    );
  }
}

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    editItem: (
      id: number,
      campaignName: string,
      name: string,
      email: string,
      subject: string,
      emailTemplate: string,
      mailerName: string
    ) =>
      dispatch(
        actions.editItemFromList(
          id,
          campaignName,
          name,
          email,
          subject,
          emailTemplate,
          mailerName
        )
      ),
    notifyEditMsg: (msg: boolean) =>
      dispatch(actions.displayEditNotification(msg)),
    fetchTitle: (title: string) =>
      dispatch(actions.fetchSelectedTabTitle(title)),
  };
};

export default connect(null, mapDispatcherToProps)(EditExistingForm);
