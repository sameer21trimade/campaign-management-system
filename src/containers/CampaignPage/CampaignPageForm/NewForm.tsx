import React, { Component } from "react";
import "./CampaignPageForm.scss";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";
import { NavLink } from "react-router-dom";
import Button from "../../../components/Button/Button";
import Input from "../../../components/InputElement/Input";
import Select from "../../../components/SelectElement/Select";

type ReduxType = ReturnType<typeof mapDispatcherToProps>;

interface props {
  campaignName: string;
}

interface stateProperties {
  userName: string;
  userNameError: string;
  userEmail: string;
  userEmailError: string;
  inputText: string;
  emailTemplate: string;
  isValid: boolean;
  isFormValid: boolean;
  top: number;
  subject: string;
  mailerName: string;
}

class NewForm extends Component<ReduxType & props, stateProperties> {
  constructor(props: ReduxType & props) {
    super(props);
    this.state = {
      userName: "",
      userNameError: "",
      userEmail: "",
      userEmailError: "",
      inputText: "",
      emailTemplate: "",
      isValid: false,
      isFormValid: false,
      top: -100,
      subject: "",
      mailerName: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.inputEmailTemplate = this.inputEmailTemplate.bind(this);
    this.rejectForm = this.rejectForm.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(({
      [event.target.name]: event.target.value,
    } as unknown) as stateProperties);
    this.validateData();
  };

  inputEmailTemplate(event: React.ChangeEvent<HTMLSelectElement>) {
    this.setState({ emailTemplate: event.target.value });
  }

  inputMailerName(event: React.ChangeEvent<HTMLSelectElement>) {
    this.setState({ mailerName: event.target.value });
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    this.validate();
  }

  handleClick() {
    const isValid = this.validate();
    if (isValid) {
      this.props.addItem(
        this.props.campaignName,
        this.state.userName,
        this.state.userEmail,
        this.state.subject,
        this.state.emailTemplate,
        this.state.mailerName
      );
      this.props.notificationMsg(true);
      this.props.fetchTitle("Recent");
    }
  }

  rejectForm() {
    this.props.fetchTitle("Recent");
  }

  onInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ inputText: event.target.value });
  }

  validateData = () => {
    if (this.state.userName.length > 0 && this.state.userEmail.includes("@")) {
      this.setState({ isFormValid: true });
    } else {
      this.setState({ isFormValid: false });
    }
  };

  validate = () => {
    let userNameError = "";
    let userEmailError = "";

    if (!this.state.userName) {
      userNameError = "*Name cannot be blank";
    }

    if (!this.state.userEmail.includes("@")) {
      userEmailError = "*Invalid email";
    }

    if (userEmailError || userNameError) {
      this.setState({ userEmailError: userEmailError });

      this.setState({ userNameError: userNameError });
      return false;
    }
    this.setState({ isValid: true });
    return true;
  };

  render() {
    const { userNameError, userEmailError, isFormValid } = this.state;
    return (
      <>
        <div className="form_container">
          <form onSubmit={this.handleSubmit}>
            <h3>{this.props.campaignName}</h3>
            <h2>To:</h2>
            <div className="dropdown_container">
              <p>Mailer Name</p>
              <Select
                onChange={(event: any) => this.inputMailerName(event)}
                defaultValue=""
                disabledValue="-- select an mailer name --"
                optionArray={["AN-test1", "test1", "mailer2"]}
              />
            </div>
            <h2>From:</h2>
            <div className="input_field_container border_bottom">
              <div className="required_fields">
                <p>Name*</p>
                <Input
                  onChange={(event: any) => this.handleOnChange(event)}
                  type="text"
                  name="userName"
                  placeholder="Enter name"
                  value={this.state.userName}
                />
                <small>{userNameError}</small>
              </div>
              <div className="required_fields">
                <p>Email*</p>
                <Input
                  onChange={(event: any) => this.handleOnChange(event)}
                  type="text"
                  name="userEmail"
                  placeholder="Enter email"
                  value={this.state.userEmail}
                />
                <small>{userEmailError}</small>
              </div>
            </div>
            <div className="input_field_container">
              <div>
                <p>subject</p>
                <Input
                  onChange={(event: any) => this.handleOnChange(event)}
                  type="text"
                  name="subject"
                  placeholder="Enter subject"
                  value={this.state.subject}
                />
              </div>
              <div className="dropdown_container">
                <p>Email template</p>
                <Select
                  onChange={(event: any) => this.inputEmailTemplate(event)}
                  defaultValue=""
                  disabledValue="-- select a template --"
                  optionArray={[
                    "Review email template",
                    "Invoice email template",
                    "Follow up email template",
                    "Sales email template",
                  ]}
                />
              </div>
            </div>
            <div className="send_and_cancel_buttons">
              {isFormValid ? (
                <NavLink to="/campaigns">
                  <Button
                    buttonName="Send Campaign"
                    onClick={() => this.handleClick()}
                    buttonType="button"
                    buttonStyle="button_primary"
                  />
                </NavLink>
              ) : (
                <Button
                  buttonName="Send Campaign"
                  onClick={undefined}
                  buttonType="submit"
                  buttonStyle="button_primary"
                />
              )}
              <NavLink to="/campaigns">
                <Button
                  buttonName="Cancel"
                  onClick={() => this.rejectForm()}
                  buttonType="button"
                  buttonStyle="button_secondary"
                />
              </NavLink>
            </div>
          </form>
        </div>
      </>
    );
  }
}

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    addItem: (
      campaignName: string,
      name: string,
      email: string,
      subject: string,
      emailTemplate: string,
      mailerName: string
    ) =>
      dispatch(
        actions.addItemToList(
          campaignName,
          name,
          email,
          subject,
          emailTemplate,
          mailerName
        )
      ),
    notificationMsg: (msg: boolean) =>
      dispatch(actions.displayNotification(msg)),
    fetchTitle: (title: string) =>
      dispatch(actions.fetchSelectedTabTitle(title)),
  };
};

export default connect(null, mapDispatcherToProps)(NewForm);
