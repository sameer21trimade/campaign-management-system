import React, { Component } from "react";
import "./CampaignPageHeader.scss";
import ReactModal from "react-modal";
import Header from "../../../components/Header/Header";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";
import { InitialRootState } from "../../../Redux/store/Root/Index";
import Tabs from "../../Navigationtabs/TabsContainer/TabsContainer";
import SingleTab from "../../Navigationtabs/SingleTab/SingleTab";
import CampaignDetails from "../CampaignTable/CampaignTable";
import Pagination from "../../../components/Pagination/Pagination";
import searchButton from "../../../images/searchButton.svg";
import Notification from "./Notification";
import Input from "../../../components/InputElement/Input";
import Modal from "../../../components/Modal/Modal";
import Button from "../../../components/Button/Button";
import { NavLink } from "react-router-dom";

type ReduxType = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatcherToProps>;

interface stateProperties {
  campaignName: string;
  campaignNameError: string;
  isValid: boolean;
  search: string;
  currentPage: number;
  listItemsPerPage: number;
  top: number;
  isOpen: boolean;
  minLength: string;
  displayClearButton: boolean;
}

ReactModal.setAppElement("#root");
class CampaignsView extends Component<ReduxType, stateProperties> {
  constructor(props: ReduxType) {
    super(props);
    this.state = {
      campaignName: "",
      campaignNameError: "",
      isValid: false,
      search: "",
      currentPage: 1,
      listItemsPerPage: 5,
      top: -100,
      isOpen: false,
      minLength: "",
      displayClearButton: false,
    };
    this.gotoPreviousPage = this.gotoPreviousPage.bind(this);
    this.gotoNextPage = this.gotoNextPage.bind(this);
    this.searchCampaign = this.searchCampaign.bind(this);
    this.inputName = this.inputName.bind(this);
    this.createCamapign = this.createCamapign.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.validate = this.validate.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
  }

  createCamapign() {
    this.setState({ isOpen: true });
  }

  closeModal() {
    this.setState({ isOpen: false, campaignNameError: "" });
    this.setState({ campaignName: "" });
    this.setState({ minLength: "" });
  }

  submitForm(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    this.validate();
    if (this.state.campaignName.length === 1) {
      this.props.notificationMsg(false);
      this.setState({ isValid: false });
      this.setState({
        minLength: "*Campaign Name should contain more than 1 characters",
      });
      this.setState({ campaignNameError: "" });
    }
  }

  inputName(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ campaignName: event.target.value }, () => {
      if (this.state.campaignName.length > 1) {
        this.validate();
        this.props.notificationMsg(true);
        this.setState({ minLength: "" });
      }
      if (this.state.campaignName.length === 1) {
        this.props.notificationMsg(false);
        this.setState({ isValid: false });
        this.setState({ campaignNameError: "" });
      }
    });
  }

  validate() {
    let nameError = "";
    if (!this.state.campaignName) {
      nameError = "*Name cannot be blank";
    }
    if (nameError) {
      this.setState({ campaignNameError: nameError });
      this.setState({ minLength: "" });
      return false;
    }
    this.setState({ isValid: true });
    return true;
  }

  searchCampaign(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({ search: event.target.value }, () => {
      this.props.searchValue(false);
      if (this.state.search === "")
        this.setState({ displayClearButton: false });
    });
    this.setState({ displayClearButton: true });
  }

  clearSearch() {
    this.setState({ search: "" }, () => {
      this.props.searchValue(false);
    });
    this.setState({ displayClearButton: false });
  }

  gotoPreviousPage() {
    if (this.state.currentPage !== 1) {
      this.setState((previousState) => {
        return { currentPage: previousState.currentPage - 1 };
      });
    }
  }

  gotoNextPage(index: any) {
    if (this.state.currentPage !== index.length) {
      this.setState((previousState) => {
        return { currentPage: previousState.currentPage + 1 };
      });
    }
  }

  render() {
    const {
      currentPage,
      listItemsPerPage,
      isOpen,
      campaignNameError,
      isValid,
      minLength,
    } = this.state;
    let indexOfLastPost = currentPage * listItemsPerPage;
    let indexOfFirstPost = indexOfLastPost - listItemsPerPage;
    let currentListItems = this.props.list.slice(
      indexOfFirstPost,
      indexOfLastPost
    );
    let length = this.props.list.length;

    const paginate = (pageNumber: any) => {
      this.setState({ currentPage: pageNumber });
    };
    if (this.props.selectedTabTitle) {
      let temporaryArray: any[] = [];
      this.props.list.forEach((index) => {
        if (index.status === this.props.selectedTabTitle) {
          temporaryArray.push(index);
        }
      });
      currentListItems = temporaryArray.slice(
        indexOfFirstPost,
        indexOfLastPost
      );
      length = temporaryArray.length;
    }
    if (this.props.selectedTabTitle === "Recent") {
      currentListItems = this.props.list.slice(
        indexOfFirstPost,
        indexOfLastPost
      );
      length = this.props.list.length;
    }
    if (this.state.search === "") {
      this.props.searchValue(false);
    }
    if (this.props.onChangeTab) {
      paginate(1);
      this.setState({ search: "", displayClearButton: false });
    }
    return (
      <>
        <Header />
        <Notification />
        <div className="title_container">
          <h2 id="title">Campaigns</h2>
          <div className="input_container">
            <div className="search_campaigns_container">
              <Input
                onChange={(event: any) => this.searchCampaign(event)}
                type="text"
                name="searchCampaign"
                placeholder="Search Campaigns"
                value={this.state.search}
              />
              {this.state.displayClearButton ? (
                <Button
                  buttonName="Clear"
                  buttonType="button"
                  onClick={() => this.clearSearch()}
                  buttonStyle="clear"
                />
              ) : undefined}

              <div className="search_icon">
                <img src={searchButton} alt="searchButton" />
              </div>
            </div>
            <Button
              buttonName="Create Campaign"
              buttonType="button"
              onClick={() => this.createCamapign()}
              buttonStyle="button_primary"
            />
            <Modal isOpen={isOpen}>
              <form onSubmit={this.submitForm}>
                <div className="modal_container">
                  <div className="modal_header">
                    <h4>Create Campaign</h4>
                    <Button
                      buttonName="X"
                      buttonType="button"
                      onClick={() => this.closeModal()}
                      buttonStyle="button_secondary"
                    />
                  </div>
                  <h4 id="name">
                    <span id="warning">*</span>
                    <span>Name</span>
                  </h4>
                  <div className="user_name_container">
                    <Input
                      onChange={(event: any) => this.inputName(event)}
                      type="text"
                      name={undefined}
                      placeholder="Enter name"
                      value={this.state.campaignName}
                    />
                    <small>{minLength}</small>
                    <small>{campaignNameError}</small>
                  </div>
                  <div className="modal_buttons">
                    <Button
                      buttonName="Cancel"
                      buttonType="button"
                      onClick={() => this.closeModal()}
                      buttonStyle="modal_secondary"
                    />
                    {isValid ? (
                      <NavLink to={`/campaignName/${this.state.campaignName}`}>
                        <Button
                          buttonName="Create"
                          onClick={undefined}
                          buttonType="submit"
                          buttonStyle="modal_primary"
                        />
                      </NavLink>
                    ) : (
                      <Button
                        buttonName="Create"
                        onClick={undefined}
                        buttonType="submit"
                        buttonStyle="modal_primary"
                      />
                    )}
                  </div>
                </div>
              </form>
            </Modal>
          </div>
        </div>
        <Tabs>
          <SingleTab title="Recent">
            <CampaignDetails
              listArray={currentListItems}
              title="Recent"
              search={this.state.search}
            />
          </SingleTab>
          <SingleTab title="Sent">
            <CampaignDetails
              listArray={currentListItems}
              title="Sent"
              search={this.state.search}
            />
          </SingleTab>
          <SingleTab title="Scheduled">
            <CampaignDetails
              listArray={currentListItems}
              title="Scheduled"
              search={this.state.search}
            />
          </SingleTab>
          <SingleTab title="Draft">
            <CampaignDetails
              listArray={currentListItems}
              title="Draft"
              search={this.state.search}
            />
          </SingleTab>
        </Tabs>
        <Pagination
          listItemsPerPage={listItemsPerPage}
          totalLists={length}
          paginate={paginate}
          gotoPreviousPage={this.gotoPreviousPage}
          gotoNextPage={this.gotoNextPage}
        />
      </>
    );
  }
}

const mapStateToProps = ({ rootReducer }: InitialRootState) => {
  const {
    list,
    onChangeTab,
    selectedTabTitle,
    notification,
    searchInput,
  } = rootReducer;
  return { list, onChangeTab, selectedTabTitle, notification, searchInput };
};

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    notificationMsg: (msg: boolean) =>
      dispatch(actions.displayNotification(msg)),
    searchValue: (noCampaignFound: boolean) =>
      dispatch(actions.filterCampaigns(noCampaignFound)),
  };
};

export default connect(mapStateToProps, mapDispatcherToProps)(CampaignsView);
