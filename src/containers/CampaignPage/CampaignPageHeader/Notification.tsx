import React, { Component } from "react";
import "./CampaignPageHeader.scss";
import { connect } from "react-redux";
import { InitialRootState } from "../../../Redux/store/Root/Index";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";

type ReduxType = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatcherToProps>;

interface stateProperties {
  top: number;
}

class Notification extends Component<ReduxType, stateProperties> {
  constructor(props: ReduxType) {
    super(props);
    this.state = {
      top: -100,
    };
  }

  componentDidMount() {
    if (this.props.notification) {
      this.setState({ top: 16 }, () => {
        setTimeout(() => {
          this.setState({ top: -100 });
          this.props.notificationMsg(false);
        }, 2000);
      });
    }
    if (this.props.editMsg) {
      this.setState({ top: 16 }, () => {
        setTimeout(() => {
          this.setState({ top: -100 });
          this.props.notifyEditMsg(false);
        }, 2000);
      });
    }
  }

  render() {
    return (
      <>
        {this.props.editMsg ? (
          <>
            <div className="toast_container" style={{ top: this.state.top }}>
              Camapign Edited Successfully
            </div>
          </>
        ) : undefined}
        {this.props.notification ? (
          <>
            <div className="toast_container" style={{ top: this.state.top }}>
              Camapign Sent Successfully
            </div>
          </>
        ) : undefined}
      </>
    );
  }
}

const mapStateToProps = ({ rootReducer }: InitialRootState) => {
  const { notification, editMsg } = rootReducer;
  return { notification, editMsg };
};

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    notifyEditMsg: (msg: boolean) =>
      dispatch(actions.displayEditNotification(msg)),
    notificationMsg: (msg: boolean) =>
      dispatch(actions.displayNotification(msg)),
  };
};

export default connect(mapStateToProps, mapDispatcherToProps)(Notification);
