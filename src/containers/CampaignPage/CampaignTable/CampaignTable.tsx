import React, { Component } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";
import { InitialRootState } from "../../../Redux/store/Root/Index";
import "./CampaignTable.scss";
import RouterLink from "./RouterLink";
import Button from "../../../components/Button/Button";
import Tooltip from "../../../components/Tooltip/Tooltip";
import Modal from "../../../components/Modal/Modal";
import deleteOption from "../../../images/deleteOption.svg";

interface Props {
  title: string;
  search: string;
  listArray: Object[];
}

interface stateProperties {
  top: number;
  isOpen: boolean;
  id: number;
}

type ReduxType = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatcherToProps>;

class campaignDetails extends Component<ReduxType & Props, stateProperties> {
  constructor(props: ReduxType & Props) {
    super(props);
    this.state = {
      top: -100,
      isOpen: false,
      id: 0,
    };
    this.deleteCampaign = this.deleteCampaign.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  deleteCampaign(id: number) {
    this.setState({ isOpen: true });
    this.setState({ id: id });
  }

  deleteItem() {
    this.setState({ isOpen: false });
    this.props.deleteItem(this.state.id);
    this.props.notificationMsg(true);
    this.setState({ top: 16 }, () => {
      setTimeout(() => {
        this.setState({ top: -100 });
        this.props.notificationMsg(false);
      }, 2000);
    });
  }

  closeModal() {
    this.setState({ isOpen: false });
  }

  render() {
    const { title, listArray, notification } = this.props;
    const { isOpen } = this.state;
    return (
      <>
        {notification ? (
          <>
            <div className="toast_container" style={{ top: this.state.top }}>
              Camapign Deleted Successfully
            </div>
          </>
        ) : undefined}
        <div className="campaigns_table_container">
          <table className="list_each_campaign">
            <tbody>
              <tr id="heading_row">
                <th>Campaign</th>
                <th>Status</th>
                <th>Opens</th>
                <th>Clicks</th>
                <th>Actions</th>
              </tr>
            </tbody>
            {listArray // eslint-disable-next-line
              .filter((data: any) => {
                if (this.props.search === null) return data;
                else {
                  if (
                    data.campaignName
                      .toLowerCase()
                      .includes(this.props.search.toLowerCase())
                  ) {
                    return data;
                  } else {
                    this.props.searchValue(true);
                  }
                }
              })
              .map((index: any) => {
                return (
                  <>
                    {index.status === title || title === "Recent" ? (
                      <tbody>
                        <tr className="campaign_data">
                          <td id="campaign_name">{index.campaignName}</td>
                          <td>
                            <p id="status">{index.status}</p>
                          </td>
                          <td>{index.opens}</td>
                          <td>{index.clicks}</td>
                          <td>
                            <RouterLink
                              id={index.id}
                              campaignName={index.campaignName}
                              userName={index.userName}
                              userEmail={index.userEmail}
                              subject={index.subject}
                              emailTemplate={index.emailTemplate}
                              mailerName={index.mailerName}
                            />
                            <Tooltip tooltiptext="Delete Campaign">
                              <img
                                src={deleteOption}
                                onClick={() => this.deleteCampaign(index.id)}
                                alt="delete option"
                              />
                            </Tooltip>
                            <Modal isOpen={isOpen}>
                              <div className="modal">
                                <h3>
                                  Are you sure you want to delete this item ?
                                </h3>
                                <div className="delete_option">
                                  <Button
                                    buttonName="Yes"
                                    onClick={() => this.deleteItem()}
                                    buttonType="button"
                                    buttonStyle="button_primary"
                                  />
                                  <Button
                                    buttonName="No"
                                    onClick={() => this.closeModal()}
                                    buttonType="button"
                                    buttonStyle="button_primary"
                                  />
                                </div>
                              </div>
                            </Modal>
                          </td>
                        </tr>
                      </tbody>
                    ) : undefined}
                  </>
                );
              })}
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = ({ rootReducer }: InitialRootState) => {
  const { notification } = rootReducer;
  return { notification };
};

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    deleteItem: (id: number) => dispatch(actions.removeItemFromList(id)),
    notificationMsg: (msg: boolean) =>
      dispatch(actions.displayNotification(msg)),
    searchValue: (noCampaignFound: boolean) =>
      dispatch(actions.filterCampaigns(noCampaignFound)),
  };
};

export default connect(mapStateToProps, mapDispatcherToProps)(campaignDetails);
