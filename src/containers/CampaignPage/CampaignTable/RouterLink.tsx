import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";
import editOption from "../../../images/editOption.svg";
import Tooltip from "../../../components/Tooltip/Tooltip";

interface Props {
  id: number;
  campaignName: string;
  userName: string;
  userEmail: string;
  subject: string;
  emailTemplate: string;
  mailerName: string;
}

type ReduxType = ReturnType<typeof mapDispatcherToProps>;

class RouterLink extends Component<ReduxType & Props> {
  constructor(props: ReduxType & Props) {
    super(props);
    this.editCampaign = this.editCampaign.bind(this);
  }

  editCampaign() {
    this.props.notifyEditMsg(true);
  }

  render() {
    const {
      id,
      campaignName,
      userName,
      userEmail,
      subject,
      emailTemplate,
      mailerName,
    } = this.props;
    return (
      <>
        <Link
          onClick={() => this.editCampaign()}
          to={`/createCampaignID/${id}/${campaignName}/${userName}/${userEmail}/${subject}/${emailTemplate}/${mailerName}`}
        >
          <Tooltip tooltiptext="Edit Campaign">
            <img src={editOption} alt="edit option" />
          </Tooltip>
        </Link>
      </>
    );
  }
}

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    notifyEditMsg: (msg: boolean) =>
      dispatch(actions.displayEditNotification(msg)),
  };
};

export default connect(null, mapDispatcherToProps)(RouterLink);
