import React, { Component } from "react";
import Header from "../../components/Header/Header";
import { connect } from "react-redux";
import { InitialRootState } from "../../Redux/store/Root/Index";
import "./Dashboard.scss";

type ReduxType = ReturnType<typeof mapStateToProps>;

class Dashboard extends Component<ReduxType> {
  render() {
    const { list } = this.props;
    const todaysDate = new Date().toLocaleDateString();
    return (
      <>
        <Header />
        <div className="dashboard_container">
          <h2 className="dashboard_title">Dashboard Activity</h2>
          <h2 className="date">
            <span>{todaysDate}</span>
          </h2>
          <div className="campaign_list">
            {list.map((index: any) => {
              return (
                <div>
                  {index.date === new Date().toLocaleDateString() ? (
                    <li key={index.id}>
                      <span className="time">{index.time}</span>
                      <span>{index.campaignName}</span>
                    </li>
                  ) : undefined}
                </div>
              );
            })}
            <h2 className="date">
              <span>10/10/2020</span>
            </h2>
            {list.map((index: any) => {
              return (
                <div>
                  {index.date === "10/10/2020" ? (
                    <li key={index.id}>
                      <span className="time">{index.time}</span>
                      <span>{index.campaignName}</span>
                    </li>
                  ) : undefined}
                </div>
              );
            })}
            <h2 className="date">
              <span>06/10/2020</span>
            </h2>
            {list.map((index: any) => {
              return (
                <div>
                  {index.date === "06/10/2020" ? (
                    <li key={index.id}>
                      <span className="time">{index.time}</span>
                      <span>{index.campaignName}</span>
                    </li>
                  ) : undefined}
                </div>
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = ({ rootReducer }: InitialRootState) => {
  const { list } = rootReducer;
  return { list };
};

export default connect(mapStateToProps)(Dashboard);
