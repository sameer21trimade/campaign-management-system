import React, { Component } from "react";

interface Props {
  title: string;
  children: any;
}

class SingleTab extends Component<Props> {
  render() {
    return <div>{this.props.children}</div>;
  }
}

export default SingleTab;
