import React, { Component } from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../../../Redux/store/Actions/Actions";
import { Actions } from "../../../Redux/store/Types/Types";
import { NavLink } from "react-router-dom";
import "./TabsContainer.scss";

interface Props {
  children: any;
}

interface componentState {
  selectedTab: number;
  checkIndex: number;
}

type ReduxType = ReturnType<typeof mapDispatcherToProps>;

class Tabs extends Component<Props & ReduxType, componentState> {
  constructor(props: Props & ReduxType) {
    super(props);
    this.state = {
      selectedTab: 0,
      checkIndex: 0,
    };
    this.onClick = this.onClick.bind(this);
  }
  onClick(title: any, index: any) {
    this.props.changePageNumber(true);
    this.props.fetchTitle(title);
    this.setState({ checkIndex: index });
  }

  render() {
    return (
      <div className="nav_tabs">
        <ul>
          <nav>
            {this.props.children.map((item: any, index: any) => (
              <>
                {this.state.checkIndex === index ? (
                  <NavLink
                    to="#"
                    style={{ borderBottom: "2px solid #4490fb" }}
                    onClick={() => this.onClick(item.props.title, index)}
                  >
                    <li>{item.props.title}</li>
                  </NavLink>
                ) : (
                  <NavLink
                    to="#"
                    onClick={() => this.onClick(item.props.title, index)}
                  >
                    <li>{item.props.title}</li>
                  </NavLink>
                )}
              </>
            ))}
          </nav>
        </ul>
        {this.props.children[this.state.selectedTab]}
      </div>
    );
  }
}

const mapDispatcherToProps = (dispatch: Dispatch<Actions>) => {
  return {
    changePageNumber: (value: boolean) => dispatch(actions.onChangeTab(value)),
    fetchTitle: (title: string) =>
      dispatch(actions.fetchSelectedTabTitle(title)),
  };
};

export default connect(null, mapDispatcherToProps)(Tabs);
